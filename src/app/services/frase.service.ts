
import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Frase } from '../model/frase';

@Injectable({
  providedIn: 'root'
})
export class FraseService {

  url: string = "https://catfact.ninja/fact";

  constructor(private _http: HttpClient) { }

  getFrase(){
    return this._http.get<Frase>(this.url);



  }

}
