import { Component, ComponentFactoryResolver, ViewChild, ViewContainerRef } from '@angular/core';
import { FraseapiComponent } from './fraseapi/fraseapi.component';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {


@ViewChild('container', {read: ViewContainerRef}) container: ViewContainerRef;

constructor( private ComponentFactoryResolver: ComponentFactoryResolver){}


  loadConponente(componente:string){

    this.container.remove();
    let componentFactory;

    if (componente === 'frases'){

    componentFactory = this.ComponentFactoryResolver.resolveComponentFactory(FraseapiComponent);
    }
    const component = this.container.createComponent(componentFactory);


    }

}
