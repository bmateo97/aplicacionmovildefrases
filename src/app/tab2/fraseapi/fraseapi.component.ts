
import { Component, OnInit } from '@angular/core';
import { FraseService } from 'src/app/services/frase.service';
import { Frase } from 'src/app/model/frase';

@Component({
  selector: 'app-fraseapi',
  templateUrl: './fraseapi.component.html',
  styleUrls: ['./fraseapi.component.scss'],
})
export class FraseapiComponent implements OnInit {


  frase: Frase = new Frase();
  getFrase: Frase[] = [];

    constructor(private FraseService: FraseService) { }

    getfrases() {
//metodo
      this.FraseService.getFrase().subscribe((data: Frase) => {
          this.frase = data;
          this.getFrase.push(this.frase);
          console.log(this.getFrase);
        }
      );

    }


    ngOnInit() {

      this.getfrases();
    }
  }
